Word locator allows user to check whether the specified (re)occurrence of a query word appears in the text file.

Compile: g++ -o wl wl.cpp

When started, it display a prompt">", waiting for commands.

load, locate, new, end are valid commands of this program.

load command accepts an input file and initialise vectors. if input file argument is missing or invalid file is entered, it will return ERROR: invalid argument
example: >load valid.txt 
		  loaded
		 >load
		 ERROR:invalid argument

locate command accepts a word to locate and the number of occurrence of the word in the file. it will return ERROR: invalid argument 
if arguments are missing or if number of occurrence number is not a number
if the specified word is not found it will print "no matching entry"
example:
		>locate dog 3 //search for third occurrence of word dog in the file (1)
		>locate sing 2 //search for second occurrence of word sing in the file (2)
		in the text "this dog is unwell, let take this dog to..." in the first locate example it will print no matching entry
		in the second locate example it will return "8" the dog is in 8th position starting from 1

new command resets all the vectors and maps

end command terminates the program
