#include <cstdio>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <iterator>
#include <string>
#include <cctype>
using namespace std;

string removePunct(string &word) {
	word.erase(remove_if(word.begin(),word.end(),::ispunct),word.end());	
	return word;	
}

string toLower(string &words) {
	transform(words.begin(),words.end(),words.begin(),::tolower);
		return words;
}

//read from a given file and load words to words vector
void loadFile(const vector<string> &command,vector<string> &words) {
	if (command.size() != 2) {
		cout << "ERROR: Invalid command";
		return;
	}
	//open file	
    fstream input (command.at(1));
	if (!input.is_open()) {
		cout <<"ERROR: Invalid command";
	}
	//copy from file to words vector
	copy(istream_iterator<string>(input),istream_iterator<string>(),back_inserter(words));
	//make all words to lower case
	transform(words.begin(),words.end(),words.begin(),toLower);
	//remove punctuation from words
	transform(words.begin(),words.end(),words.begin(),removePunct);
}

//seaches for a word in the map if found returns its number in line in the input file or returns
//its previous occurence line if any else return 0
int map_lookup(const unordered_map<string, vector<int>> &pairs,const string &word,int n,int &occurence) {
	auto result = pairs.find(word);
	//if word not found return zero
	if (result == pairs.end()) {
		return 0;
	}
	// if v.size == n then return word location or if v.size != n then find word location until i == n
	auto v = result->second;
	if (v.size() == n) {
		occurence = n;
		return v[n-1];
	} else {
		int line = 0;
		for(int i = 0; i < v.size();i++) {
			if (i == n)
				break;
			line = i;
		}
		occurence = line+1;
		return v[line];
	}
}
//locates the word in the file for example in the text "this is a pen" it returns pen = 4
void locate(const vector<string> &commandArgs,vector<string> &words,unordered_map<string,vector<int>> &pairs) {
	int n;
	if ( (commandArgs.size() != 3) || (commandArgs.at(1) == "") ||  ((n = strtol(commandArgs.at(2).c_str(),NULL,10)) == 0)) {
		cout <<"ERROR: Invalid command";
		return;
	}
	int occurence = 0;
	//searches the word in the map, if found returns the word location 
	int startIndex = map_lookup(pairs, commandArgs[1],  n,occurence);
	if(startIndex != 0 && occurence == n) {
		cout << startIndex;
		return;
	}
	// if not in map then search for word in a linear way in the array and if found add it to map and print word location or if not found print entry not found
	int wordLine = 0;
	for(int i = startIndex;i < words.size(); ++i) {
		//if word occurence is equal than requested occurence(n) then print word location and break
		if (occurence == n){
			wordLine = i;
			cout << wordLine;
			break;
		}
		if(words[i] == commandArgs[1]) {
			occurence++;
			int k = i;
			pairs[commandArgs[1]].push_back(++k); 
		}
	}
	if (wordLine == 0)
		cout << "No matching entry";
	
}

int main() {
	vector<string> words;
	string command;
	vector<string> commandArgs;
	unordered_map<string, vector<int>> wordOccurence;
	while(1) {
		do {
			cout << ">";
			getline(cin,command);
	   	} while(command.length() == 0);
		commandArgs.clear();

		//command string to all lower
		transform(command.begin(),command.end(),command.begin(),::tolower);
		//stringstring to eliminate whitespace and copy to commandArgs vector
		stringstream s(command);
		copy(istream_iterator<string>(s),istream_iterator<string>(),back_inserter(commandArgs));
		if (commandArgs.at(0) == "load") {
			loadFile(commandArgs, words);
			cout << "loaded\n";	
		} else if (commandArgs.at(0) == "locate") {
			locate(commandArgs, words, wordOccurence);
			cout << "\n";	
		} else if ((commandArgs[0] == "new") && (commandArgs.size() == 1)) {
			wordOccurence.clear();
			commandArgs.clear();
			words.clear();
		} else if (commandArgs[0] == "end") {
			break;
		} else{
			cout <<"ERROR: Invalid command" << endl;
		}

	}	
}
